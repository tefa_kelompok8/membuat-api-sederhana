class Query {
  constructor(db) {
    this.db = db;
  }

  async findContent(parameter) {
    const { id } = parameter;
    const recordset = await this.db.prepareQuery('SELECT title, uploader, rating, total_user, details, material FROM content WHERE id = ?', [id]);
    return recordset;
  }

  async duplicateContent(parameter) {
    const { title, uploader, rating, totalUser, details, material } = parameter;
    const recordset = await this.db.prepareQuery(
      'SELECT title, uploader, rating, total_user, details, material FROM content WHERE title = ? AND uploader = ? AND rating = ? AND total_user = ? AND details = ? AND material = ?',
      [title, uploader, rating, totalUser, details, material]
    );
    return recordset;
  }
}

module.exports = Query;
