const Content = require('./domain');
const Mysql = require('../../../../helpers/databases/mysql/db');
const config = require('../../../../infra/configs/global_config');
const db = new Mysql(config.get('/mysqlConfig'));
const content = new Content(db);

const getContent = async (id) => {
  const getData = async () => {
    const result = await content.viewContent(id);
    return result;
  };
  const result = await getData();
  return result;
};

module.exports = {
  getContent,
};
