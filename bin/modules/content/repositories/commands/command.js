const { deleteContent } = require("./command_handler");

class Command {
  constructor(db) {
    this.db = db;
  }

  async insertContent(document) {
    const { id, title, uploader, rating, totalUser, details, material } = document;
    const result = await this.db.prepareQuery('INSERT INTO content (id, title, uploader, rating, total_user, details, material ) VALUES (?, ?, ?, ?, ?, ?, ?)', [
      id,
      title,
      uploader,
      rating,
      totalUser,
      details,
      material,
    ]);
    return result;
  }

  async putContent(document) {
    const { id, title, uploader, rating, totalUser, details, material } = document;
    const result = await this.db.prepareQuery('UPDATE content set title = ?, uploader = ?, rating = ?,  total_user=?, details=?, material=? WHERE id=?', [
      title,
      uploader,
      rating,
      totalUser,
      details,
      material,
      id,
    ]);
    return result;
  }

  async deleteContent(parameter) {
    const recordset = await this.db.prepareQuery('DELETE FROM content WHERE id = ?', parameter);
    return recordset;
  }
}


module.exports = Command;
